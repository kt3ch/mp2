import React,  { Component } from 'react'; 
import { Link } from 'react-router-dom';



class ListView extends Component {
  state = {}


  render() {
    let size_limit = {
      width: "auto",
      height: "auto"
    }
    return (
<div>
      <div class="card mb-3" >
        <div class="row no-gutters">
        <div class="col-md-4">
        <Link
          to={{
            pathname: process.env.PUBLIC_URL + '/detailview/' + this.props.id,
            state: { id: this.props.id }
          }}
        >
          <img src={`https://image.tmdb.org/t/p/w200${this.props.poster_path}`} class="card-img" style={size_limit} alt=""/>
          </Link>
        </div>
        <div class="col-md-8">
          <div class="card-body">
            <h5 class="card-title">{this.props.title}</h5>
            <li class="list-group-item">Release Date: {this.props.release_date}</li>
            <li class="list-group-item">Popularity: {this.props.popularity}</li>
            <li class="list-group-item">Average Ratings: {this.props.vote_average}</li>
          </div>
        </div>
        </div>
      </div>
      </div>
    );
  }
}
export default ListView;