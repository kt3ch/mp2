import React from 'react';

import './styles.scss';
import ListView from '../ListView'
import axios from 'axios';
import { Link } from "react-router-dom";
const base_url = "https://api.themoviedb.org/3/"
const api_key = "&api_key=1441ae36da77ea112c211d1c31cde806"

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      search_word: "",
      filter: "",
      sortOn: "popularity",
      order: "desc"
    }
    
  }
  search = () => {
    var request = base_url + 'search/multi?query= ' + this.state.search_word + api_key + this.state.filter;
    axios.get(request).then(response => {
      this.setState({
        movies: response.data.results.sort(
          (x, y) => {
            if (this.state.sortOn === "popularity") {
              return this.state.order === "asc" ? x.popularity - y.popularity : y.popularity - x.popularity;
            } else if (this.state.sortOn === "vote_average") {
              return this.state.order === "asc" ? x.vote_average - y.vote_average : y.vote_average - x.vote_average;
            } else {
              return this.state.order === "asc" ? new Date(x.release_date) - new Date(y.release_date) : new Date(y.release_date) - new Date(x.release_date);
            }
          }
        )
      })
    });
  };

  updateSearch(w) {
    this.setState({
      search_word: w
    });
    this.search();
  }

  updateOrder(w) {
    this.setState({
      order: w
    });
    this.search();
  }
  updateSortOn(w) {
    this.setState({
      sortOn: w
    });
    this.search();
  }
  updateFilter(w) {
    this.setState({
      filter: w
    });
    this.search();
  }
  

  render() {
    let my_style = {
      marginTop: '50px',
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '75%'
    }
    return (
      <div className="search">
        <nav className="navbar sticky-top navbar-light bg-dark">
				<Link to={{ pathname: process.env.PUBLIC_URL + "/search" }} className="btn m-2 btn-primary btn-sm active">
            Search
          </Link>
        <Link to={{ pathname: process.env.PUBLIC_URL + "/" }} className="btn m-2 btn-primary btn-sm active">
            Gallery
          </Link>
				</nav>


        <div className="input-group" style={my_style}>
          <input type="search" value={this.state.search_word} onChange={c => this.updateSearch(c.target.value)} className="form-control"/>
          <div class="input-group-append">
            <span class="input-group-text">Search</span>
          </div>
          <div className="splitter"/>



          <div>
          <span class="badge badge-secondary">Sort By</span>
            <select value={this.state.sortOn} onChange={e => this.updateSortOn(e.target.value)}>
              <option value="popularity">Popularity</option>
              <option value="vote_averagge">Ratings</option>
              <option value="release_date">Release Date</option>
            </select>
          </div>



          <div>
          <span class="badge badge-secondary">Order By</span>
            <select value={this.state.order} onChange={e => this.updateOrder(e.target.value)}>
              <option value="desc">Descending</option>
              <option value="asc">Ascending</option>
            </select>
          </div>



          <div>
            <b>Language</b>
            <select value={this.state.filter} onChange={e => this.updateFilter(e.target.value)}>
              <option value="&language=en-US">English</option>
              <option value="">Any Language</option>
            </select>
          </div>





        </div>
        <div className="movies-list-container">
          {this.state.movies.map(movie => (
            <ListView {...movie} key={movie.id} />
          ))}
        </div>
      </div>
    );
  }
}


export default Search;