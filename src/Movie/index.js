import React, { Component } from "react";
import { Link } from "react-router-dom";
import './styles.scss'

class Movie extends Component {
  state = {};

  render() {
    let card = {
      width: '13rem'
    };
  
    return (
      <div className="card" style={card} >
        <Link to={{
            pathname: process.env.PUBLIC_URL+ '/detailview/' + this.props.id,
            state: { id: this.props.id }
          }}>
          <img src={`https://image.tmdb.org/t/p/w200${this.props.poster_path}`} alt=""/>
        </Link>
        <div className="card-body">
        <p className="card-text">{this.props.original_title}</p>
        </div>
      </div>
    );
  }
}

export default Movie;