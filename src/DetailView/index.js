import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './styles.scss'
const base_url = "https://api.themoviedb.org/3/"
const image_base = "https://image.tmdb.org/t/p/w500/"
const api_key = "?api_key=1441ae36da77ea112c211d1c31cde806"
class DetailView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movie: {}
    }
  }

  getDetails() {
    var request = base_url + 'movie/' + this.props.location.state.id + api_key;
    axios.get(request).then(response => {
      this.setState({
        movie: response.data
      });
    });
  };

  render() {
    this.getDetails();
    let card = {
      width: '75%',
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto'
    }
    return (
      <div className="details">
        <nav className="navbar sticky-top navbar-light bg-dark">
				<Link to={{ pathname: process.env.PUBLIC_URL+ "/search/" }} className="btn m-2 btn-primary btn-sm active">
            Search
        	</Link>
        <Link to={{ pathname: process.env.PUBLIC_URL+"/" }} className="btn m-2 btn-primary btn-sm active">
            Gallery
        	</Link>
				</nav>





        <div class="card d-flex justify-content-start" style={card}>
          <img src={image_base + this.state.movie.poster_path} class="card-img-top" alt=""/>
          <div class="card-body">
          <h5 class="card-title">{this.state.movie.title}</h5>
          <p class="card-text">{this.state.movie.overview}</p>
          
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Release Data: {this.state.movie.release_date}</li>
            <li class="list-group-item">Popularity: {this.state.movie.popularity}</li>
            <li class="list-group-item">Average Ratings: {this.state.movie.vote_average}</li>
            <li class="list-group-item">Homepage: <a href={this.state.movie.homepage}>{this.state.movie.homepage}</a> </li>
            <p class="list-group-item">{this.state.movie.tagline}</p>
          </ul>
          <div class="card-body">
          <Link
            to={{
              pathname: process.env.PUBLIC_URL+ '/detailview/' + this.state.movie.id - 1,
              state: {
                id: `${this.state.movie.id - 1}`
              }
            }}
            className="card-link"
          >Prev</Link>
          <Link
            to={{
              pathname: process.env.PUBLIC_URL+ '/detailview/' + this.state.movie.id + 1,
              state: {
                id: this.state.movie.id + 1
              }
            }}
            className="card-link"
          >
            Next
          </Link>
          </div>
          </div>
      </div>
    )
  }
}

export default DetailView;