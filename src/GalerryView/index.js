import React from 'react';
import './styles.scss';
import { Link } from "react-router-dom";
import axios from "axios";
import Movie from '../Movie'
import './styles.scss'
import {  PropTypes } from 'prop-types';
const base_url = "https://api.themoviedb.org/3/"
const api_key = "&api_key=1441ae36da77ea112c211d1c31cde806"
class GalleryView extends React.Component {
  constructor(props) {
		super(props)
		this.state = {
			movies: []
		};
		this.getPopular()
	}
	
	genres = {
    Action: 28,
    Adventure: 12,
    Animation: 16,
    Comedy: 35,
		Crime: 80,
		Documentary: 99,
		Drama: 18,
		Family: 10753,
		Fantasy: 14,
		History: 36,
		Horror: 27,
		Music: 10402,
		Mystery: 9648,
		Romance: 10749,
		SciFi: 878,
		TvMovie: 10770,
		Thriller: 53,
		War: 10752,
		Western: 37
	};
	getGenre = gen => {
		var my_request = base_url + 'discover/movie?sort_by=popularity.desc&include_video=false&page=1&with_genres=' + gen + api_key;
		axios.get(my_request).then(response => {
			this.setState({
				movies: response.data.results
			});
		});
		// axios.get(my_request)
	}

	getPopular = () => {
		var my_request = base_url + 'movie/popular?' + api_key;
		axios.get(my_request).then(response => {this.setState({movies: response.data.results})});
	}
	mountGenre(genre) {
		this.getGenre(this.genres[genre]);
	}
	mountPopular() {
		this.getPopular();
	}
	render() {
		return (
			<div className="gallery">
				<nav className="navbar sticky-top navbar-light bg-dark">
				<Link to={{ pathname: process.env.PUBLIC_URL + "/search" }} className="btn m-2 btn-primary btn-sm active">
            Search
        	</Link>
        <Link to={{ pathname: process.env.PUBLIC_URL+ "/" }} className="btn m-2 btn-primary btn-sm active">
            Gallery
        	</Link>
				</nav>

				<div className="genres-bar">
          {Object.keys(this.genres).map(genre => (
            <button
              key={genre}
              onClick={() => this.mountGenre(genre)}
              className="btn btn btn-outline-dark m-1 btn"
            >
              {genre}
            </button>
          ))}
        </div>
				<div className="movies-container">
          {this.state.movies.map(movie => (
            <Movie {...movie} key={movie.id} />
          ))}
        </div>
			</div>
		)
	}

}
GalleryView.propTypes = {
  state: PropTypes.element.isRequired
};

export default GalleryView;


