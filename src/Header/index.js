import React from 'react';
import './styles.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";
class Header extends React.Component {
  render() {
    return (
      <div className="navbar">
         <Router>
           <ul>
         <li>
            <Link to="/List View">List View</Link>
          </li>
          <li>
            <Link to="/Galerry View">Galerry View</Link>
          </li>
          </ul>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/List View">
            <About />
          </Route>
          <Route path="/Galerry View">
            <Dashboard />
          </Route>
        </Switch>
          </Router>
          
      </div>
      );
    }
}

function Home() {
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
}

function About() {
  return (
    <div>
      <h2>About</h2>
    </div>
  );
}

function Dashboard() {
  return (
    <div>
      <h2>Dashboard</h2>
    </div>
  );
}

export default Header;