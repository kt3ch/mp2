import React, { Component } from 'react';

import './App.scss';
import Search from './Search';
import Gallery from './GalerryView';
import DetailView from "./DetailView"
import {
  BrowserRouter as Router,
  Switch,
  Route
  // Link,
  // useParams
} from "react-router-dom";

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route exact path={process.env.PUBLIC_URL + "/"} component={Gallery} />
            <Route path={process.env.PUBLIC_URL + "/detailview/:id"} component={DetailView} /> 
            <Route path={process.env.PUBLIC_URL + "/search"} component={Search} />
          </Switch>
        </Router>

    );
  }
}


export default App;
